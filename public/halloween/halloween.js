/**
 * This is the "source" for the ascii art files in this same folder. It's partially minified by hand,
 * but hopefully still somewhat readable.
 *
 * Run it through a proper minifier (such as uglifyJS) and then to get the base64 run in a terminal:
 *     cat | base64 -w 0
 *
 * Paste the minified code in, enter a newline and press ctrl-d
 */

{
  R = (r = 1) => +Math.random().toFixed(3) * r;
  P = (p) => R() < p;
  f = 36;
  s = ".___,_______,_____Happy_Halloween____.\n|1./(7)\\.8|13|\n|1)2\\/\\_/\\/2(8|13|\n|1`)2(^Y^)2(`6\\(|)/11|\n|2`),-(~)-,(`6--(&)--10|\n|6'\"'6\\\\4/`\\12|\n|10.-'```^```'-.4,5,2|\n|9/3(\\1__1/)2\\3)\\___/(2|\n|9|4`1\\/1`3|2{(@)v(@)}1|\n|9\\4\\____/3/3{|~~~|}2|\n|10`'-.......-'`4{/^^^\\}2|\n.____________________________`m-m`___.".replace(
    /\d+/g,
    (d) => " ".repeat(d)
  );

  b = document.body;
  b.style =
    "background:#111;display:flex;flex-direction:row;height:100vh;justify-content:center;margin:0;align-items:center;font-family:monospace;overflow:hidden";
  b.removeAttribute("class");
  b.innerHTML =
    `<pre style="margin:0;padding:0;max-height:initial;background:#111;color:#eee;font-weight:bold;font-size:${f}px">` +
    s
      .replace(
        "^Y^",
        '<span class="b" style="color:#f80">^</span>Y<span class="b" style="color:#f80">^</span>'
      )
      .replace("(&)", '(<span class="s" style="color:red">&</span>)')
      .replace(
        /\(\\[ /\\_]+\)|`[/ \\]+`| \\_+\//g,
        (p) => `<span class="p" style="color:#f80">${p}</span>`
      )
      .replaceAll("@", '<span class="o" style="color:#ff0">@</span>') +
    '</pre><canvas style="position:fixed;top:0;left:0"></canvas>';

  $ = (q) => document.querySelector(q);
  $$ = (q) => [...document.querySelectorAll(q)];
  bs = $$(".b");
  ps = $$(".p");
  os = $$(".o");
  s = document.querySelector(".s");

  ls = [];
  v = $("canvas");
  x = v.getContext("2d");
  c = 0;
  z = () => ((v.width = innerWidth), (v.height = innerHeight));

  z();

  window.addEventListener("resize", z);

  h = (h) => R(h * 2) - h;

  for (n = 0; n < 50; n++)
    ls.push({
      x: R(v.width),
      y: R(-2 * v.height),
      ys: 0.5 + R(2),
      xs: 0,
      t: "\ud83c" + (P(0.66) ? (P(0.5) ? "\udf43" : "\udf41") : "\udf42"),
      r: R(Math.PI),
      rs: 0,
    });

  !(function e() {
    requestAnimationFrame(e);

    c = ++c % 10;

    if (c == 0) {
      r = P(0.03);
      bs.map((b) => (b.style.opacity = r ? 0 : 1));

      g = ((5 + R(11)) | 0).toString(16);
      ps.map((p) => (p.style.color = `#f${g}0`));

      os.map((o) => (o.style.opacity = P(0.03) ? 0 : 1));

      s.style.opacity = P(0.03) ? 0.5 : 1;
    }

    x.font = `${f * 0.5}px monospace`;
    x.textBaseline = "middle";
    x.clearRect(0, 0, v.width, v.height);

    for (l of ls) {
      l.y += l.y >= v.height ? 0 : l.ys;

      if (l.y >= v.height) {
        if (P(0.003)) {
          l.y = -f;
          l.x = R(v.width);
        }
      } else {
        l.xs = P(0.02) ? h(1) : l.xs;
        l.x = Math.max(0, Math.min(v.width, l.x + l.xs));

        l.rs = P(0.01) ? h(0.02) : l.rs;
        l.r += l.rs;
      }

      x.translate(l.x, l.y);
      x.rotate(l.r);
      x.fillText(l.t, 0, 0);
      x.rotate(-l.r);
      x.translate(-l.x, -l.y);
    }
  })();
}
