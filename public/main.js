// @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD-3-Clause

async function main() {
  const { title, availableArt, ...info } = await getInfo();

  document.title = title;

  if (typeof info.css === "string") {
    info.css = [info.css];
  }

  if (Array.isArray(info.css)) {
    for (const href of info.css) {
      const link = document.createElement("link");
      link.rel = "stylesheet";
      link.href = href;
      document.head.appendChild(link);
    }
  }

  const requestedArt = new URLSearchParams(location.search).get("art");
  const artName = availableArt.includes(requestedArt)
    ? requestedArt
    : availableArt[(availableArt.length * Math.random()) | 0];

  const pre = document.querySelector("pre");
  const loading = document.querySelector(".loading");
  const main = document.querySelector(".main");
  const alert = document.querySelector(".alert");
  const copyBtn = document.querySelector("#copy");
  const runBtn = document.querySelector("#run");
  const randomBtn = document.querySelector("#random");
  let alertTimeout = null;

  copyBtn.addEventListener("click", () =>
    copy(pre)
      .then(() => {
        alert.textContent =
          "Copied to clipboard!\nYou can paste this into the devtools of pages to run anywhere.";
      })
      .catch(() => {
        alert.textContent = "Issue copying to clipboard!";
      })
      .then(() => {
        clearTimeout(alertTimeout);
        alert.removeAttribute("hidden");
        alertTimeout = setTimeout(() => {
          alert.setAttribute("hidden", "hidden");
        }, 5000);
      })
  );

  runBtn.addEventListener("click", () => eval(pre.textContent));

  randomBtn.addEventListener("click", async () => {
    main.setAttribute("hidden", "hidden");
    loading.removeAttribute("hidden");

    await setCode(pre, availableArt[(availableArt.length * Math.random()) | 0]);

    loading.setAttribute("hidden", "hidden");
    main.removeAttribute("hidden");
  });

  await setCode(pre, artName);
  loading.setAttribute("hidden", "hidden");
  main.removeAttribute("hidden");
}

function getInfo() {
  return fetch("./info.json").then((r) => r.json());
}

async function getArt(artName) {
  if (!("_cache" in getArt)) {
    getArt._cache = new Map();
  }

  if (!getArt._cache.has(artName)) {
    getArt._cache.set(
      artName,
      fetch(artName + ".txt").then((r) => r.text())
    );
  }

  return getArt._cache.get(artName);
}

async function getCode(artName) {
  const art = await getArt(artName);
  const lines = art.trimEnd().split("\n");

  return `<span class="code">eval(atob(</span>
${lines
  .map(
    (l, i) =>
      `<span class="code">'</span>${l}<span class="code">'${
        i < lines.length - 1 ? "+" : ""
      }</span>`
  )
  .join("\n")}<span class="code">))</span>`;
}

async function setCode(element, artName) {
  const code = await getCode(artName);

  element.innerHTML = code;
}

function copy(element) {
  const selection = window.getSelection();
  const range = document.createRange();
  range.selectNodeContents(element);
  selection.removeAllRanges();
  selection.addRange(range);

  if (navigator.clipboard) {
    return navigator.clipboard.writeText(element.textContent);
  }

  return new Promise((resolve, reject) => {
    try {
      if (document.queryCommandSupported("copy")) {
        document.execCommand("copy");
        resolve();
      } else {
        reject(new Error("Unable to copy to clipboard"));
      }
    } catch (e) {
      reject(e);
    }
  });
}

main();

// @license-end
