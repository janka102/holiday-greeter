/**
 * This is the "source" for the ascii art files in this same folder. It's partially minified by hand,
 * but hopefully still somewhat readable.
 *
 * Run it through a proper minifier (such as uglifyJS) and then to get the base64 run in a terminal:
 *     cat | base64 -w 0
 *
 * Paste the minified code in, enter a newline and press ctrl-d
 */

{
  R = (r = 1) => +Math.random().toFixed(3) * r;
  P = (p) => R() < p;
  f = 36;
  s = `11Merry1Christmas!


16{^}
16/1\\
15/3\\
15/3\\
14/5\\11_
3/\\8/7\\9/1\\
2/2\\7/7\\8/3\\
2/2\\6/9\\7/3\\
1/4\\4/11\\5/5\\
/6\\2/13\\3/7\\`
    .replace(/\d+/g, (d) => " ".repeat(d))
    .replace(/ (?= *\\)/gm, (e) => (P(0.4) ? "*" : e));

  b = document.body;
  b.style =
    "background:#fff;display:flex;flex-direction:row;height:100vh;justify-content:center;margin:0;align-items:end;font-family:monospace;overflow:hidden";
  b.removeAttribute("class");
  b.innerHTML =
    `<pre style="margin:0;padding:0;max-height:initial;background:#fff;color:#000;font-weight:bold;font-size:${f}px">` +
    s
      .replace(/[/\\_]/g, (e) => `<span style="color:#080">${e}</span>`)
      .replace(
        /\*/g,
        (e) =>
          `<span class="o" style="color:${P(0.4) ? "red" : "#ccc"}">*</span>`
      )
      .replace("{^}", (e) => '<span class="a" style="color:gold">{^}</span>') +
    '</pre><canvas style="position:fixed;top:0;left:0"></canvas>';

  $ = (q) => document.querySelector(q);
  os = [...document.querySelectorAll(".o")];
  a = $(".a");
  w = [];
  p = $("pre");
  v = $("canvas");
  x = v.getContext("2d");
  c = 0;
  z = () => ((v.width = innerWidth), (v.height = innerHeight));

  z();

  window.addEventListener("resize", z);

  for (n = 0; n < 200; n++)
    w.push({
      x: R(v.width),
      y: R(-2 * v.height),
      speed: 1 + R(2),
    });

  !(function e() {
    requestAnimationFrame(e);

    c = ++c % 5;

    if (c == 0) {
      a.style.opacity = P(0.03) ? R() : 1;

      for (o of os) o.style.opacity = P(0.01) ? R() : 1;
    }

    x.font = `${f * 0.5}px monospace`;
    x.textBaseline = "middle";
    x.fillStyle = "#69f";
    x.clearRect(0, 0, v.width, v.height);
    for (s of w) {
      s.y += s.y >= v.height ? 0 : s.speed;

      if (s.y >= v.height) {
        if (P(0.003)) {
          s.y = -f;
          s.x = R(v.width);
        }
      } else {
        s.x = Math.max(0, Math.min(v.width, s.x + (R(2) - 1)));
      }

      x.fillText("*", s.x, s.y);
    }
  })();
}
